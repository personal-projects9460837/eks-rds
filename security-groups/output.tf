output "rds_sg" {
  value = aws_security_group.rds_sg.id
}

output "bastion_sg" {
  value = aws_security_group.bastion_sg.id
}

output "jenkins_controller_sg" {
  value = aws_security_group.jenkins_controller_sg.id
}

output "jenkins_agent_sg" {
  value = aws_security_group.jenkins_agent_sg.id
}

output "sonar_sg" {
  value = aws_security_group.sonarqube_sg.id
}

output "jenkins_lb_sg" {
  value = aws_security_group.jenkins_lb_sg.id
}

output "efs_sg" {
  value = aws_security_group.efs_security_group.id
}