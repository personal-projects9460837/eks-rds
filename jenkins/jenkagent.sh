#!/bin/bash
sudo hostnamectl set-hostname jenk-agent
sudo apt update
sudo apt install openjdk-11-jdk -y
sudo apt install docker.io -y
sudo systemctl start docker
sudo systemctl enable docker
sudo usermod -aG docker ubuntu
sudo chmod 777 /var/run/docker.sock
sudo apt install unzip
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
unzip awscliv2.zip
sudo ./aws/install
curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
sudo chmod +x kubectl
sudo mv kubectl /usr/local/bin/
curl -o aws-iam-authenticator https://amazon-eks.s3.us-west-2.amazonaws.com/1.15.10/2020-02-22/bin/linux/amd64/aws-iam-authenticator
sudo chmod +x ./aws-iam-authenticator
sudo mv ./aws-iam-authenticator /usr/local/bin
sudo mkdir /home/ubuntu/.kube
sudo chown -R ubuntu:ubuntu /home/ubuntu/.kube
echo "${kubeconfig}" >> /home/ubuntu/.kube/config
sudo apt-get install -y gettext-base
sudo apt install stress
wget https://github.com/prometheus/node_exporter/releases/download/v1.7.0/node_exporter-1.7.0.linux-amd64.tar.gz
tar -xvf node_exporter-1.7.0.linux-amd64.tar.gz
rm -rf node_exporter-1.7.0.linux-amd64.tar.gz
cd node_exporter-1.7.0.linux-amd64
bash -c "./node_exporter &"
