# Create sonarqube instances
resource "aws_instance" "sonar" {
  ami                                    = var.ami_ubuntu
  instance_type                          = "t2.medium"
  subnet_id                              = var.pub_sub
  associate_public_ip_address            = true 
  vpc_security_group_ids                 = [var.sonar_sg]
  key_name                               = var.key_pair
  user_data                              = local.sonarqube_user_data
  user_data_replace_on_change            = true 

  tags = {
    Name                                 = "sonar"
  }
}