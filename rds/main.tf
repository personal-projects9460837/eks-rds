resource "aws_db_subnet_group" "rds" {
  name       = "my-rds-subnet-group"
  subnet_ids = var.priv_sub

  tags = {
    Name = "my-rds-subnet-group"
  }
}


# #create RDS(database)
resource "aws_db_instance" "rds" {
  allocated_storage           = 10
  db_subnet_group_name        = aws_db_subnet_group.rds.name
  engine                      = "mysql"
  engine_version              = "5.7"
  identifier                  = "mysql-rds"
  instance_class              = "db.t3.micro"
  multi_az                    = false
  db_name                     = "petclinic"
  username                    = jsondecode(aws_secretsmanager_secret_version.example.secret_string)["username"]
  password                    = jsondecode(aws_secretsmanager_secret_version.example.secret_string)["password"]
  storage_type                = "gp2"
  vpc_security_group_ids      = [var.rds_sg]
  skip_final_snapshot         = true
  parameter_group_name        = "default.mysql5.7"
}

resource "aws_iam_policy" "ASCP" {
  name        = "ASCP_POLICY"
  path        = "/"
  description = "policy for ASCP to access secret manager"

  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  policy = jsonencode(
    {
    "Version": "2012-10-17",
    "Statement": [
      {
        "Effect": "Allow",
        "Action": [
          "secretsmanager:GetSecretValue",
          "secretsmanager:DescribeSecret"
          ],
        "Resource": "${aws_secretsmanager_secret.eks.arn}"
      }
    ]
  }
  )
}


resource "aws_secretsmanager_secret" "eks" {
  name                    = "rds-cred"
  description             = "credentials for rds"
  recovery_window_in_days = 0

  tags = {
    Name = "rds_cred"
  }
}

resource "aws_secretsmanager_secret_version" "example" {
  secret_id     = aws_secretsmanager_secret.eks.id
  secret_string = jsonencode(var.db_cred)
}
