locals {
  sonarqube_user_data = <<-EOF
#!/bin/bash
sudo apt update
sudo hostnamectl set-hostname sonarqube
sudo apt install docker.io -y
sudo systemctl start docker
sudo systemctl enable docker
sudo usermod -aG docker ubuntu
docker run -it -d --name sonarqube -p 9000:9000 sonarqube
EOF
}

#sudo apt-get install -y postgresql-client
#psql --version  
#psql -h $(terraform output -raw rds_hostname) -p $(terraform output -raw rds_port) -U $(terraform output -raw rds_username) postgres
#ec53982b6d79