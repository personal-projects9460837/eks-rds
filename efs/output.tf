output "efs-dns-endpoint" {
  value = aws_efs_file_system.jenkins-efs.dns_name
}
