output "rds_endpoint" {
  value = aws_db_instance.rds.endpoint
}

output "credentials-key1" {
  value = jsondecode(aws_secretsmanager_secret_version.example.secret_string)["username"]
  sensitive = true
}

output "credentials-key2" {
  value = jsondecode(aws_secretsmanager_secret_version.example.secret_string)["password"]
  sensitive = true
}


output "secret-id" {
  value = aws_secretsmanager_secret.eks.id
}
output "secret-arn" {
  value = aws_secretsmanager_secret.eks.arn
}

output "policy_secret_arn" {
  value = aws_iam_policy.ASCP.arn
}