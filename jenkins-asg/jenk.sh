#!/bin/bash
sudo hostnamectl set-hostname jenkins-controller
sudo apt update
sudo apt install openjdk-11-jdk -y
curl -fsSL https://pkg.jenkins.io/debian-stable/jenkins.io-2023.key | sudo tee \
    /usr/share/keyrings/jenkins-keyring.asc > /dev/null
echo deb [signed-by=/usr/share/keyrings/jenkins-keyring.asc] \
    https://pkg.jenkins.io/debian-stable binary/ | sudo tee \
    /etc/apt/sources.list.d/jenkins.list > /dev/null
sudo apt-get update
sudo apt-get install jenkins -y
sudo apt install software-properties-common
wget -O- https://apt.releases.hashicorp.com/gpg | \
    sudo gpg --dearmor -o /usr/share/keyrings/hashicorp-archive-keyring.gpg 
echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] \
    https://apt.releases.hashicorp.com $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/hashicorp.list
sudo apt update && sudo apt install terraform 
sudo apt install nfs-common -y
sudo rm /lib/systemd/system/nfs-common.service
sudo systemctl daemon-reload
sudo systemctl start nfs-common
sudo systemctl enable nfs-common
sudo echo "${aws_instance.jenkins-agent.private_ip} jenk-agent" >> /etc/hosts
sudo mkdir -p /data
sudo chmod 755 /data
sudo mkdir -p /data/.ssh
sudo su -c "sudo mount -t nfs4 -o nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2,noresvport ${aws_efs_file_system.jenkins-efs.dns_name}:/ /data" ubuntu
sudo systemctl stop jenkins
sudo cp -r /var/lib/jenkins/ /data
sudo rm -rf /var/lib/jenkins
sudo chown -R jenkins:jenkins /data
ssh-keyscan -H jenk-agent >> /data/.ssh/known_hosts
sudo chown -R jenkins:jenkins /data/.ssh
sudo mkdir /etc/systemd/system/jenkins.service.d
echo [Service] >> /etc/systemd/system/jenkins.service.d/override.conf
echo 'Environment="JENKINS_HOME=/data"' >> /etc/systemd/system/jenkins.service.d/override.conf
echo WorkingDirectory=/data >> /etc/systemd/system/jenkins.service.d/override.conf
sudo systemctl daemon-reload
sudo systemctl start jenkins
