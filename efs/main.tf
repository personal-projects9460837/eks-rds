#creating efs
resource "aws_efs_file_system" "jenkins-efs" {
  creation_token          = "jenkins-efs"
  performance_mode        = "generalPurpose"
  throughput_mode         = "bursting"
  encrypted               = "true"

  tags = {
    Name = "Jenkins-efs"
   }
 }

# creating a mount target for the efs
 resource "aws_efs_mount_target" "jenkins-efs-mt" {       
  count            = 3       #to mount to more than one subnets
  file_system_id   = aws_efs_file_system.jenkins-efs.id
  subnet_id        = var.subnet-id[count.index] 
  security_groups  = [var.efs-sg]
 }