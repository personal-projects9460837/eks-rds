output "certificate_arn" {
  value = aws_acm_certificate.acm_certificate.arn
}

output "domain_name" {
  value = var.domain_name
}

# output "route53_nameservers" {
#   value = data.aws_route53_zone.hosted_zone.name_servers
# }