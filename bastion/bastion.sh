#!/bin/bash
echo "${priv_key}" >> /home/ubuntu/.ssh/myKey
sudo mkdir /home/ubuntu/.kube
echo "${kubeconfig}" >> /home/ubuntu/.kube/config
chown -R ubuntu:ubuntu /home/ubuntu/.ssh/myKey
chmod 400 /home/ubuntu/.ssh/myKey
sudo hostnamectl set-hostname Bastion-host
sudo apt install unzip
#installing awscli
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
unzip awscliv2.zip
sudo ./aws/install
#insttalling kubectl
curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl
#installing aws-iam-authenticator
curl -o aws-iam-authenticator https://amazon-eks.s3.us-west-2.amazonaws.com/1.15.10/2020-02-22/bin/linux/amd64/aws-iam-authenticator
sudo chmod +x ./aws-iam-authenticator
sudo mv ./aws-iam-authenticator /usr/local/bin
#transfering iam-user credentials
sudo su -c "aws configure set aws_access_key_id ${access_key}"  ubuntu
sudo su -c "aws configure set aws_secret_access_key ${secret_key}" ubuntu
sudo su -c "aws configure set default.region eu-west-2" ubuntu
sudo su -c "aws configure set default.output json" ubuntu
#transfering files secretspc.yaml
echo "${file("./bastion/java-app-deployment.yaml")}" >> /home/ubuntu/java-app-deployment.yaml
echo "${file("./bastion/secretspc.yaml")}" >> /home/ubuntu/secretspc.yaml
echo "${file("./bastion/sec-role.yaml")}" >> /home/ubuntu/sec-role.yaml
echo "${file("./bastion/ingress.yaml")}" >> /home/ubuntu/ingress.yaml
echo "${file("./bastion/phpmyadmin.yaml")}" >> /home/ubuntu/phpmyadmin.yaml
echo "${file("./bastion/cluster-issuer-http01")}" >> /home/ubuntu/cluster-issuer-http01.yaml
echo "${file("./bastion/cluster-issuer-dns01")}" >> /home/ubuntu/cluster-issuer-dns01.yaml
#installing eksctl
ARCH=amd64
PLATFORM=$(uname -s)_$ARCH
curl -sLO "https://github.com/eksctl-io/eksctl/releases/latest/download/eksctl_$PLATFORM.tar.gz"
curl -sL "https://github.com/eksctl-io/eksctl/releases/latest/download/eksctl_checksums.txt" | grep $PLATFORM | sha256sum --check
tar -xzf eksctl_$PLATFORM.tar.gz -C /tmp && rm eksctl_$PLATFORM.tar.gz
sudo mv /tmp/eksctl /usr/local/bin
#install helm
sudo snap install helm --classic
sudo su -c "helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx" ubuntu
sudo su -c "helm repo update" ubuntu
sudo su -c "eksctl create iamserviceaccount --region="eu-west-2" --name "java-sa" --cluster "myapp-eks-cluster" --attach-policy-arn "${policy_secret_arn}" --override-existing-serviceaccounts --approve" ubuntu   