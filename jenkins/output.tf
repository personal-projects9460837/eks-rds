output "ecr_repo_url" {
  value = aws_ecr_repository.ecr.repository_url
}

output "ecr_name" {
  value = aws_ecr_repository.ecr.name
}

output "jenkins_agent_priv_IP" {
  value = aws_instance.jenkins-agent.private_ip
}
