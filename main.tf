locals {
  kubeconfig = <<KUBECONFIG
apiVersion: v1
clusters:
- cluster:
    server: ${module.eks.cluster_endpoint}
    certificate-authority-data: ${module.eks.cluster_certificate_authority_data}
  name: kubernetes
contexts:
- context:
    cluster: kubernetes
    user: aws
  name: aws
current-context: aws
kind: Config
preferences: {}
users:
- name: aws
  user:
    exec:
      apiVersion: client.authentication.k8s.io/v1beta1
      args:
      - --region
      - eu-west-2
      - eks
      - get-token
      - --cluster-name
      - "${module.eks.cluster_name}"
      - --output
      - json
      command: aws
KUBECONFIG
}

module "key_pair" {
  source = "./keypair"
}

module "security_gp" {
  source = "./security-groups"
  vpc_id = module.myapp-vpc.vpc_id
  sg_cidr = var.sg_cidr
}

module "bastion" {
  source = "./bastion"
  ami_ubuntu = module.key_pair.ami_ubuntu
  priv_key   = module.key_pair.priv_key
  key_pair   = module.key_pair.pub_key
  access_key = var.access_key
  secret_key = var.secret_key
  vpc_id     = module.myapp-vpc.vpc_id
  pub_sub    = module.myapp-vpc.public_subnets[0]
  bastion_sg = module.security_gp.bastion_sg
  kubeconfig = local.kubeconfig
  policy_secret_arn = module.rds.policy_secret_arn
}

module "sonar" {
  source     = "./sonarqube"
  ami_ubuntu = module.key_pair.ami_ubuntu
  key_pair   = module.key_pair.pub_key
  pub_sub    = module.myapp-vpc.public_subnets[0]
  sonar_sg   = module.security_gp.sonar_sg
}

module "jenkins" {
  source                = "./jenkins"
  ami_ubuntu            = module.key_pair.ami_ubuntu
  key_pair              = module.key_pair.pub_key
  priv_sub              = module.myapp-vpc.private_subnets[0]
  instance_profile_name = module.iam.instance_profile_name
  jenkins_agent_sg      = module.security_gp.jenkins_agent_sg
  ecr_name              = var.ecr_name
  kubeconfig            = local.kubeconfig 
  
}

module "jenkins_lb" {
  source = "./jenkins-lb"
  lb_sg = module.security_gp.jenkins_lb_sg
  pub_subnet_1 = module.myapp-vpc.public_subnets[0]
  pub_subnet_2 = module.myapp-vpc.public_subnets[1]
  vpc_id = module.myapp-vpc.vpc_id
  certificate_arn = module.route53.certificate_arn

}

module "jenkins_asg" {
  source                = "./jenkins-asg"
  priv_subnet1          = module.myapp-vpc.private_subnets[0]
  priv_subnet2          = module.myapp-vpc.private_subnets[1]
  jenkins_sg            = module.security_gp.jenkins_controller_sg
  jenkins_target_gp_arn = module.jenkins_lb.jenkins_target_gp_arn
  instance_type         = var.instance_type
  ami_ubuntu            = module.key_pair.ami_ubuntu
  key_pair              = module.key_pair.pub_key
  
}

module "efs_sever" {
  source    = "./efs"
  efs-sg    = module.security_gp.efs_sg
  subnet-id = module.myapp-vpc.private_subnets
}

module "rds" {
  source    = "./rds"
  db_cred   = var.db_cred
  vpc_id    = module.myapp-vpc.vpc_id
  priv_sub  = module.myapp-vpc.private_subnets
  rds_sg    = module.security_gp.rds_sg
}

module "iam" {
  source                = "./iam"
  iam_policy_name       = var.iam_policy_name
  role_name             = var.role_name
  instance_profile_name = var.instance_profile_name

}

module "route53" {
  source = "./route53"
  domain_name = var.domain_name
  domain_name2 = var.domain_name2
  jenkins_record_name = var.jenkins_record_name
  jenkins_lb_dns_name = module.jenkins_lb.jenkins_dns_name
  jenkins_lb_zone_id = module.jenkins_lb.jenkins_zone_id
  ingress_nginx_record_name = var.ingress_nginx_record_name
  ingress_nginx_lb_dns_name = data.kubernetes_service.ingress_nginx.status.0.load_balancer.0.ingress.0.hostname

}

module "myapp-vpc" {
  source               = "terraform-aws-modules/vpc/aws"
  version              = "5.5.2"

  name                 = "myapp-vpc"
  cidr                 = var.vpc_cidr_block
  private_subnets      = var.private_subnet_cidr_blocks
  public_subnets       = var.public_subnet_cidr_blocks 
  azs                  = data.aws_availability_zones.azs.names

  enable_nat_gateway   = true
  single_nat_gateway   = true
  enable_dns_hostnames = true

  tags = {
    "kubernetes.io/cluster/myapp-eks-cluster" = "shared"
  }

  public_subnet_tags = {
    "kubernetes.io/cluster/myapp-eks-cluster" = "shared"
    "kubernetes.io/role/elb" = 1
  }

  private_subnet_tags = {
    "kubernetes.io/cluster/myapp-eks-cluster" = "shared"
    "kubernetes.io/role/internal-elb" = 1
  }
}

module "eks" {
  source                          = "terraform-aws-modules/eks/aws"
  version                         = "19.17.2"

  cluster_name                    = "myapp-eks-cluster"
  cluster_version                 = "1.29"
  cluster_endpoint_public_access  = true
 # cluster_endpoint_private_access = true

  subnet_ids                      = module.myapp-vpc.private_subnets
  vpc_id                          = module.myapp-vpc.vpc_id


  cluster_security_group_additional_rules = {
    ingress_bastion_host = {
      description = "allow bastion host access to the eks cluster"
      protocol    = "tcp"
      from_port   = 443
      to_port     = 443
      type        = "ingress"
      source_security_group_id = module.security_gp.bastion_sg
    }
    ingress_jenk_agent = {
      description = "allow jenkins agent access to the eks cluster"
      protocol    = "tcp"
      from_port   = 443
      to_port     = 443
      type        = "ingress"
      source_security_group_id = module.security_gp.jenkins_agent_sg
    }
    
  }

  enable_irsa = true

  tags = {
    environment = "development"
    application = "myapp"
  }

  eks_managed_node_groups = {
    dev = {
      min_size     = 1
      max_size     = 6
      desired_size = 2

      instance_types = ["t2.medium"]
    }
  }
}

locals {
  env_name = "dev"
}

module "ebs_csi_irsa_role" {
  source = "terraform-aws-modules/iam/aws//modules/iam-role-for-service-accounts-eks"

  role_name             = "${local.env_name}-ebs-csi-role"
  attach_ebs_csi_policy = true

  oidc_providers = {
    ex = {
      provider_arn               = module.eks.oidc_provider_arn
      namespace_service_accounts = ["kube-system:ebs-csi-controller-sa"]
    }
  }
}

module "eks_blueprints_addons" {
  source = "aws-ia/eks-blueprints-addons/aws"
  version = "~> 1.0" #ensure to update this to the latest/desired version

  cluster_name      = module.eks.cluster_name
  cluster_endpoint  = module.eks.cluster_endpoint
  cluster_version   = module.eks.cluster_version
  oidc_provider_arn = module.eks.oidc_provider_arn

  eks_addons = {
    aws-ebs-csi-driver = {
      most_recent = true
      service_account_role_arn = module.ebs_csi_irsa_role.iam_role_arn
    }
    
    coredns = {
      most_recent = true
    }
    vpc-cni = {
      most_recent = true
    }
    kube-proxy = {
      most_recent = true
    }
  } 


  #enable_argocd                                = true
  enable_metrics_server                        = true 
  enable_cluster_autoscaler                    = true
  #enable_kube_prometheus_stack                 = true
  enable_secrets_store_csi_driver              = true
  secrets_store_csi_driver = {
    set = [{
            name  = "syncSecret.enabled"
            value = "true"
           },
           {
             name  = "enableSecretRotation"
             value = "true"
            }
    ]
  }
  enable_secrets_store_csi_driver_provider_aws = true
  enable_cert_manager                          = true
  enable_ingress_nginx                         = true
  ingress_nginx   = {
    name          = "ingress-nginx"
    chart_version = "4.6.1"
    repository    = "https://kubernetes.github.io/ingress-nginx"
    namespace     = "ingress-nginx"
    values        = [templatefile("${path.module}/values.yaml", {})]
  }

  tags = {
    Environment = "dev"
  }
}

resource "local_file" "dummy" {
  content  = "dummy content"
  filename = "${path.module}/dummy.txt"

  depends_on = [module.eks_blueprints_addons]
}

data "kubernetes_service" "ingress_nginx" {
  metadata {
    name      = "ingress-nginx-controller"
    namespace = "ingress-nginx"
  }

  depends_on = [local_file.dummy]
}
