output "instance_profile_name" {
  value = aws_iam_instance_profile.jenkins_profile.name
}
output "role_arn" {
  value = aws_iam_role.jenkins_role.arn
}