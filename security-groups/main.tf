# create security group for jenkins load-balancer
resource "aws_security_group" "jenkins_lb_sg" {
  name = "loadbalancer security group"
  description = "Allow inbound traffic from port 80 and 443"
  vpc_id = var.vpc_id

  ingress {
    description      = "http access"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = [var.sg_cidr]
  }

  ingress {
    description      = "https access"
    from_port        = 443
    to_port          = 443
    protocol         = "tcp"
    cidr_blocks      = [var.sg_cidr]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = [var.sg_cidr]
  }

  tags = {
    Name = "bastion_security_group"
  }
  
}
# create security group for bastion host
resource "aws_security_group" "bastion_sg" {
  name        = "bastion security group"
  description = "allow access on ports 22"
  vpc_id      = var.vpc_id 

  # allow access on port 22
  ingress {
    description = "ssh access"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [var.sg_cidr]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = [var.sg_cidr]
  }

  tags = {
    Name = "bastion_security_group"
  }
}
# Security Group for MySQL RDS Database
resource "aws_security_group" "rds_sg" {
  name        = "RDSMySQL"
  description = "Allow inbound traffic"
  vpc_id      = var.vpc_id

  ingress {
    description      = "Allow MySQL access"
    from_port        = 3306
    to_port          = 3306
    protocol         = "tcp"
    cidr_blocks      = [var.sg_cidr]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = [var.sg_cidr]
  }

  tags = {
    Name = "MySQL_RDS"
  }
}

# create security group for jenkins controller
resource "aws_security_group" "jenkins_controller_sg" {
  name        = "jenkins contr security group"
  description = "allow access on ports 22, 80, 443 and 8080"
  vpc_id      = var.vpc_id

  # allow access on port 22
  ingress {
    description = "ssh access"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    security_groups = [ aws_security_group.bastion_sg.id ]
  }

  # allow access on port 80
  ingress {
    description     = "http access"
    from_port       = 80
    to_port         = 80
    protocol        = "tcp"
    security_groups = [aws_security_group.jenkins_lb_sg.id]
  }
  
  # allow access on port 443
  ingress {
    description         = "https access"
    from_port           = 443
    to_port             = 443
    protocol            = "tcp"
    security_groups     = [aws_security_group.jenkins_lb_sg.id]
  }


  ingress {
    description = "allow access on port 8080"
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = [var.sg_cidr]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = [var.sg_cidr]
  }

  tags = {
    Name = "jenk_controller_security_group"
  }
}

# create security group for jenkins agent
resource "aws_security_group" "jenkins_agent_sg" {
  name        = "jenkins agent security group"
  description = "allow access on ports 22"
  vpc_id      = var.vpc_id

  # allow access on port 22
  ingress {
    description = "ssh access"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    security_groups = [ aws_security_group.bastion_sg.id, aws_security_group.jenkins_controller_sg.id ]
  }

  ingress {
    description = "ssh access"
    from_port   = 9100
    to_port     = 9100
    protocol    = "tcp"
    cidr_blocks = [var.sg_cidr]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = [var.sg_cidr]
  }

  tags = {
    Name = "jenkins_agent_security_group"
  }
}

# create security group for sonarqube
resource "aws_security_group" "sonarqube_sg" {
  name        = "sonarqube security group"
  description = "allow access on ports 22 and 9000"
  vpc_id      = var.vpc_id

  # allow access on port 22
  ingress {
    description = "ssh access"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    security_groups = [ aws_security_group.bastion_sg.id ]
  }

  # allow access on port 9000
  ingress {
    description = "ssh access"
    from_port   = 9000
    to_port     = 9000
    protocol    = "tcp"
    cidr_blocks = [var.sg_cidr]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = [var.sg_cidr]
  }

  tags = {
    Name = "sonarqube_security_group"
  }
}

# create security group for the efs instance
resource "aws_security_group" "efs_security_group" {
  name        = "efs security group"
  description = "allow access from jenkins-instance security group into efs"
  vpc_id      = var.vpc_id

# allow access on port 2049
  ingress {
    description = "access from port 2049"
    from_port   = 2049
    to_port     = 2049
    protocol    = "tcp"
    #security_groups = [ aws_security_group.jenkins_sg.id ]
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "efs_security_group"
  }
}