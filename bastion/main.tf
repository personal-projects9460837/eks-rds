resource "aws_instance" "bastion-server" {
  ami                         = var.ami_ubuntu
  instance_type               = "t2.micro"
  associate_public_ip_address = true
  key_name                    = var.key_pair
  subnet_id                   = var.pub_sub
  vpc_security_group_ids      = [ var.bastion_sg ]
  user_data_replace_on_change = true
  user_data                   = templatefile("./bastion/bastion.sh", {
    priv_key    = var.priv_key
    access_key  = var.access_key
    secret_key  = var.secret_key
    kubeconfig  = var.kubeconfig
    policy_secret_arn = var.policy_secret_arn
  })

  tags = {
    Name = "bastion-server"
  }
}
