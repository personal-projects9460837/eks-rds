# Create jenkins agent instances
resource "aws_instance" "jenkins-agent" {
  ami                                    = var.ami_ubuntu
  instance_type                          = "t2.medium"
  subnet_id                              = var.priv_sub
  vpc_security_group_ids                 = [var.jenkins_agent_sg]
  key_name                               = var.key_pair
  iam_instance_profile                   = var.instance_profile_name
  user_data                              = templatefile("./jenkins/jenkagent.sh", { 
    kubeconfig                           = var.kubeconfig
  })
  user_data_replace_on_change            = true 

  tags = {
    Name                                 = "jenkins-agent"
  }
}

resource "aws_ecr_repository" "ecr" {
  name                 = var.ecr_name
  image_tag_mutability = "MUTABLE"
  force_delete         = true

  image_scanning_configuration {
    scan_on_push = true
  }
}