output "jenkins_dns_name" {
  value = aws_lb.jenkins_load_balancer.dns_name
}
output "jenkins_zone_id" {
  value = aws_lb.jenkins_load_balancer.zone_id
}

output "jenkins_target_gp_arn" {
  value = aws_lb_target_group.jenkins_target_group.arn
}


