resource "aws_autoscaling_group" "asg" {
  name                      = "jenkins_asg"
  desired_capacity          = 1
  min_size                  = 1
  max_size                  = 1
  health_check_type         = "EC2" 
  health_check_grace_period = 120
  force_delete              = true
  vpc_zone_identifier       = [var.priv_subnet1, var.priv_subnet2]
  target_group_arns         = [var.jenkins_target_gp_arn]

  launch_template {
    id      = aws_launch_template.template.id
    version = "$Latest"
  }
  
  tag {
    key                     = "Name"
    value                   = "jenkins_asg"
    propagate_at_launch     = true
  }
  
}

resource "aws_launch_template" "template" {
  name                   = "jenkins_lt"
  instance_type          = var.instance_type
  image_id               = var.ami_ubuntu
  key_name               = var.key_pair
  vpc_security_group_ids = [var.jenkins_sg]
  user_data              = filebase64("./jenkins-asg/jenk.sh")

  monitoring {
    enabled = true
  }

  tag_specifications {
    resource_type = "instance"
    tags = {
      Name   = "jenkins_lt"
      Source = "Autoscaling"
    }
  }

}

# Creating ASG Policy
resource "aws_autoscaling_policy" "jenkins-ASG-Policy" {
  name                      = "Jenkins_ASG_Policy2"
  autoscaling_group_name    = aws_autoscaling_group.asg.name
  policy_type               = "TargetTrackingScaling"
  estimated_instance_warmup = 300

  target_tracking_configuration {
    predefined_metric_specification {
      predefined_metric_type = "ASGAverageCPUUtilization"
    }
    target_value = 60.0
  }
}