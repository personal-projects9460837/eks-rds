# The map here can come from other supported configurations
# like locals, resource attribute, map() built-in, etc.
variable "db_cred" {
  type = map(string)
}
variable "rds_sg" {}
variable "vpc_id" {}
variable "priv_sub" {}