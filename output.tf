output "bastion-ip" {
  value = module.bastion.bastion_ip
}

output "rds-endpoints" {
  value = module.rds.rds_endpoint
}

output "cluster_name" {
  value = module.eks.cluster_name
}

output "secret-arn" {
  value = module.rds.secret-arn
}

output "policy_secret-arn" {
  value = module.rds.policy_secret_arn
}

output "ecr_repo_url" {
  value = module.jenkins.ecr_repo_url
}

output "certificate_arn" {
  value = module.route53.certificate_arn
}

output "jenkins_agent_role_arn" {
  value = module.iam.role_arn
}

output "jenkins_lb_dns_name" {
  value = module.jenkins_lb.jenkins_dns_name
}

output "jenkins_agents_priv_ip" {
  value = module.jenkins.jenkins_agent_priv_IP
}

output "sonar_pub_ip" {
  value = "http://${module.sonar.sonar_pub_IP}:9000"
}
output "jenkins_domain_name" {
  value = "https://${var.jenkins_record_name}"
}

output "ingress_nginx_lb_ip" {
  value = data.kubernetes_service.ingress_nginx.status.0.load_balancer.0.ingress.0.hostname
}

output "ingress_domain_name" {
  value = "https://${var.ingress_nginx_record_name}"
}

# output "ingress_nginx_full_output" {
#   description = "Full output of the ingress-nginx addon"
#   value       = module.eks_blueprints_addons.ingress_nginx.values
# }