variable "ecr_name" {}
variable "ami_ubuntu" {}
variable "priv_sub" {}
variable "key_pair" {}
variable "jenkins_agent_sg" {}
variable "instance_profile_name" {}
variable "kubeconfig" {}
