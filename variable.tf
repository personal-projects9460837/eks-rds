variable vpc_cidr_block {}
variable private_subnet_cidr_blocks {}
variable public_subnet_cidr_blocks {}
variable "access_key" {}
variable "secret_key" {}
variable "db_cred" {
  type = map(string)
}
variable "sg_cidr" {}
variable "ecr_name" {}
variable "iam_policy_name" {}
variable "role_name" {}
variable "instance_profile_name" {}
variable "instance_type" {}
variable "domain_name" {}
variable "domain_name2" {}
variable "jenkins_record_name" {}
variable "ingress_nginx_record_name" {}

